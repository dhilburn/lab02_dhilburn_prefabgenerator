﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class PrefabGenerator : MonoBehaviour
{
    public string assetpath = Application.dataPath;

	[MenuItem("Project Tools/Create Prefab")]
    public static void CreatePrefab()
    {
        GameObject[] selectedObjects = Selection.gameObjects;

        foreach (GameObject gO in Selection.gameObjects)
        {
            string name = gO.name;
            string assetPath = "Assets/" + name + ".prefab";

            if(AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)))
            {
                //Debug.Log("Asset exists");
                if (EditorUtility.DisplayDialog("Hold it!", gO.name + " already exists. Do you really want to overwrite it?", "Yes", "No"))
                {
                    Debug.Log("Overwriting . . . ");
                    CreateNewPrefab(gO, assetPath);
                }
                    
            }
            else
            {
                Debug.Log("ASSET NOT FOUND");
                CreateNewPrefab(gO, assetPath);
            }
            //Debug.Log("Name: " + gO.name + "Path: " + assetPath);
        }
    }
    
    public static void CreateNewPrefab(GameObject selected, string assetPath)
    {
        Object prefab = PrefabUtility.CreateEmptyPrefab(assetPath);
        PrefabUtility.ReplacePrefab(selected, prefab);
        AssetDatabase.Refresh();
        DestroyImmediate(selected);
        GameObject clone = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
    }
}
